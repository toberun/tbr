#! /bin/bash
# QmPJXasnQEK2nBrDR8faVjbMbeubLXUFDts84LjpobwhJ3 centos-7-dev-20240612.amd64.tar.xz
# QmSR6GYohXRdsHM6XW9dgRPdrBNMaK6MtDdVqGn2cqYueZ spack-0.20.3.tar.gz

mkdir -p rootfs
ipfs cat --progress=false QmPJXasnQEK2nBrDR8faVjbMbeubLXUFDts84LjpobwhJ3 | tar Jxf - -C rootfs
ipfs cat --progress=false QmSR6GYohXRdsHM6XW9dgRPdrBNMaK6MtDdVqGn2cqYueZ | tar zxf - -C rootfs/opt/

mv rootfs/opt/spack-0.20.3 rootfs/opt/spack
CONF=rootfs/opt/spack/etc/spack/defaults/config.yaml
sed -i 's:$spack/opt/spack:/tmp/tbr.prqkzyn:' $CONF
#sed -i 's;{architecture}/{compiler.name}-{compiler.version}/{name}-{version}-{hash};{name}/{version};' $CONF
sed -i 's;{architecture}/{compiler.name}-{compiler.version}/{name}-{version}-{hash};{name}/{version}/{hash:7};' $CONF

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc

cat << 'EOF' | chroot rootfs
export PATH=/opt/spack/bin:$PATH
spack compiler add
spack install zlib
yes | spack module tcl refresh
EOF

umount rootfs/proc
umount rootfs/dev

cat $0 > rootfs/.tbr
