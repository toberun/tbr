#! /bin/bash
# QmdnYgDLa4QW1JVNiiYMq7Qceeq1bnaNSi8trFvT379phh spack-0.20.3 bootstrap centos 7

mkdir -p rootfs
ipfs cat --progress=false QmdnYgDLa4QW1JVNiiYMq7Qceeq1bnaNSi8trFvT379phh | tar Jxf - -C rootfs

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc

cat << 'EOF' | chroot rootfs
export PATH=/opt/spack/bin:$PATH
export dontask=1
spack install jasper@1.900.1
spack install -j64 wrf@3.9.1.1 build_type=dmpar nesting=basic compile_type=em_real
yes | spack module tcl refresh
cp -a /opt/spack/share/spack/modules/linux-centos7-*/ /opt/tbr/.modules
EOF

# QmZFpW1xBNqLCApKcWmRpSedutx2zm51cW94pWybM2VFkC WPSV3.9.1.TAR.gz
ipfs cat --progress=false QmZFpW1xBNqLCApKcWmRpSedutx2zm51cW94pWybM2VFkC | tar zxf - 

mkdir -p rootfs/opt/tbr/WPS
mv WPS rootfs/opt/tbr/WPS/3.9.1

cat << 'EOF' | chroot rootfs
cd /opt/tbr/WPS/
ln -sf ../wrf/3.9.1.1/ WRFV3
cd /opt/tbr/WPS/3.9.1
mkdir include lib
cd /opt/tbr/WPS/3.9.1/include
ln -sf /opt/tbr/zlib/*/include/* .
ln -sf /opt/tbr/jasper/*/include/* .
ln -sf /opt/tbr/libpng/*/include/* .
ln -sf /opt/tbr/netcdf-c/*/include/* .
ln -sf /opt/tbr/netcdf-fortran/*/include/* .
cd /opt/tbr/WPS/3.9.1/lib
ln -sf /opt/tbr/zlib/*/lib/*.so* .
ln -sf /opt/tbr/jasper/*/lib/*.so* .
ln -sf /opt/tbr/libpng/*/lib64/*.so* .
ln -sf /opt/tbr/netcdf-c/*/lib/*.so* .
ln -sf /opt/tbr/netcdf-fortran/*/lib/*.so* .
cd /opt/tbr/WPS/3.9.1
export NETCDF=/opt/tbr/WPS/3.9.1
echo 1 | ./configure
sed -i 's;/glade/u/home/wrfhelp/UNGRIB_LIBRARIES;$(NETCDF);' configure.wps
sed -i 's/lnetcdf/lnetcdf -lnetcdff/' configure.wps 
./compile
EOF

umount rootfs/proc
umount rootfs/dev

rm -rf rootfs/opt/spack
rm -rf rootfs/root/.spack
cat $0 > rootfs/.tbr

cat << 'EOF' > rootfs/opt/tbr/.modules/wps-3.9.1
#%Module1.0

append-path --delim {:} PATH {/opt/tbr/WPS/3.9.1}
append-path --delim {:} LD_LIBRARY_PATH {/opt/tbr/WPS/3.9.1/lib}
EOF

cat << 'EOS' > rootfs/opt/tbr/bin/tobe.run
#! /bin/bash

. /etc/profile.d/modules.sh 
module use /opt/tbr/.modules
module load $(basename /opt/tbr/.modules/wrf-3.9.1.1*)
module load wps-3.9.1
exec "$@"
EOS

chmod +x rootfs/opt/tbr/bin/tobe.run

N=$(basename rootfs/opt/tbr/.modules/wrf-3.9.1.1*)
cat << EOB > $N.def
Bootstrap: localimage
From: $PWD/rootfs

EOB

cat << 'EOB' >> $N.def
%runscript
    exec /opt/tbr/bin/tobe.run "$@"
EOB

rm -f $N.sif
singularity build $N.sif $N.def
rm -f $N.def
