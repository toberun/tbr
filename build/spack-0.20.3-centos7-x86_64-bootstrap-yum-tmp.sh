#! /bin/bash

mkdir -p rootfs
# mini centos rootfs
ipfs cat --progress=false QmYdxxv6cTHSpMMhyYA9qVp3FrPqqyWhRqZUoQqUj7BRyf |tar Jxf - -C rootfs
# spack-0.20.3
ipfs cat --progress=false QmSR6GYohXRdsHM6XW9dgRPdrBNMaK6MtDdVqGn2cqYueZ |tar zxf - -C rootfs/opt/

mv rootfs/opt/spack-0.20.3 rootfs/opt/spack
CONF=rootfs/opt/spack/etc/spack/defaults/config.yaml
sed -i 's:$spack/opt/spack:/tmp/tbr.0:' $CONF
sed -i 's;{architecture}/{compiler.name}-{compiler.version}/{name}-{version}-{hash};{name}/{version};' $CONF
#sed -i 's;{architecture}/{compiler.name}-{compiler.version}/{name}-{version}-{hash};{name}/{version}/{hash:3};' $CONF

echo 'nameserver 119.29.29.29' > rootfs/etc/resolv.conf

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc

cat << 'EOF' | chroot rootfs
export PATH=/sbin:/bin:/usr/sbin:/usr/bin
yum clean all
yum -y update 
yum -y install python3 gcc gcc-c++ gcc-gfortran patch tar gzip unzip bzip2 xz file findutils hostname redhat-lsb-core environment-modules binutils which perl tcsh time
yum clean all

export PATH=/opt/spack/bin:$PATH
spack compiler add
spack install pkgconf
yes | spack module tcl refresh
EOF

umount rootfs/proc
umount rootfs/dev

cat $0 > rootfs/.tbr
