#! /bin/bash

mkdir rootfs
# QmSazvp3BwcUg86Asir1qckqX9rTzTq1LNUcvMc7LxHcx2 voidlinux.20240617.x86_64.tar.xz
ipfs cat --progress=false QmSazvp3BwcUg86Asir1qckqX9rTzTq1LNUcvMc7LxHcx2 | tar Jxf - -C rootfs
# QmTTFxhw3BL3L2zxANgj34w4fwHAnU8xnp1BH2NXwLMoQ5 tbr.el8.tar.xz
ipfs cat --progress=false QmTTFxhw3BL3L2zxANgj34w4fwHAnU8xnp1BH2NXwLMoQ5 | tar Jxf - -C rootfs/tmp
# Qmd799ZryHUBDjB1QBp3MEPTiRbau55mJKbQr7jtKGXMT8 spack-0.22.0.tar.gz
ipfs cat --progress=false Qmd799ZryHUBDjB1QBp3MEPTiRbau55mJKbQr7jtKGXMT8 | tar zxf - 
mv spack-0.22.0 rootfs/opt/spack

CONF=rootfs/opt/spack/etc/spack/defaults/config.yaml
sed -i 's:$spack/opt/spack:/tmp/tbr.el8.nt:' $CONF
sed -i 's;{architecture}/{compiler.name}-{compiler.version}/{name}-{version}-{hash};{name}/{version}_{hash:2};' $CONF
#sed -i 's;{architecture}/{compiler.name}-{compiler.version}/{name}-{version}-{hash};{name}/{version}/{hash:3};' $CONF

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc

echo nameserver 9.9.9.9 > rootfs/etc/resolv.conf

cat << 'EOF' | chroot rootfs
xbps-install -Sy python3 patch which make curl tar bzip2 gzip xz unzip git gnupg
python3 -m venv /opt/venv
/opt/venv/bin/pip install clingo
. /opt/venv/bin/activate
export PATH=/tmp/tbr.el8/usr/bin:$PATH
/opt/spack/bin/spack bootstrap now
EOF

umount rootfs/dev
umount rootfs/proc

cat $0 > rootfs/.tbr