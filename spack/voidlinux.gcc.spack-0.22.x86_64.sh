#! /bin/bash

mkdir rootfs
# QmSazvp3BwcUg86Asir1qckqX9rTzTq1LNUcvMc7LxHcx2 voidlinux.20240617.x86_64.tar.xz
ipfs cat --progress=false QmSazvp3BwcUg86Asir1qckqX9rTzTq1LNUcvMc7LxHcx2 | tar Jxf - -C rootfs
# Qmd799ZryHUBDjB1QBp3MEPTiRbau55mJKbQr7jtKGXMT8 spack-0.22.0.tar.gz
ipfs cat --progress=false Qmd799ZryHUBDjB1QBp3MEPTiRbau55mJKbQr7jtKGXMT8 | tar zxf - 
mv spack-0.22.0 rootfs/opt/spack

CONF=rootfs/opt/spack/etc/spack/defaults/config.yaml
#sed -i 's:$spack/opt/spack:/tmp/tbr.el8.nt:' $CONF
sed -i 's;{architecture}/{compiler.name}-{compiler.version}/{name}-{version}-{hash};{name}/{version}_{hash:2};' $CONF

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc

echo nameserver 9.9.9.9 > rootfs/etc/resolv.conf

cat << 'EOF' | chroot rootfs
xbps-install -Sy python3 patch which make curl tar bzip2 gzip xz unzip git gnupg autoconf automake gcc
python3 -m venv /opt/python
/opt/python/bin/pip install clingo
. /opt/python/bin/activate
/opt/spack/bin/spack bootstrap now
# xbps-remove -Ry gcc
EOF

umount rootfs/dev
umount rootfs/proc

cat $0 > rootfs/.tbr
# QmSk5Q3HBQryFVseEG1ofVNjec58mNnJ1vVkCLBLExqmko voidlinux.gcc.spack-0.22.x86_64.tar.xz