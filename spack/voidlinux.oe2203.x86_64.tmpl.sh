#! /bin/bash

mkdir rootfs
# QmQFtLP6WGQPWEXfSRPuPnUhBmiu3mVZjhsXQryzQHZHWz voidlinux.spack-0.22.x86_64.tar.xz
ipfs cat --progress=false QmQFtLP6WGQPWEXfSRPuPnUhBmiu3mVZjhsXQryzQHZHWz | tar Jxf - -C rootfs
rm -rf rootfs/tmp/*
mkdir -p rootfs/tmp/oe2203.nbg
# QmNNbUBKhYtv8bTwAgz8SwsDWAoRHvvkh6aXXHp3vu7pGD openEuler-22.03-LTS-x86_64.sysroot.tar.xz
ipfs cat --progress=false QmNNbUBKhYtv8bTwAgz8SwsDWAoRHvvkh6aXXHp3vu7pGD | tar Jxf - -C rootfs/tmp/oe2203.nbg
# QmUA3QRdymzzjqBmuNSzZYbJpFqBSDrZhxAvE5mjamx5kc patchelf-0.16.1-x86_64
ipfs cat --progress=false QmUA3QRdymzzjqBmuNSzZYbJpFqBSDrZhxAvE5mjamx5kc > rootfs/usr/local/bin/patchelf
chmod +x rootfs/usr/local/bin/patchelf

CONF=rootfs/opt/spack/etc/spack/defaults/config.yaml
sed -i 's:$spack/opt/spack:/tmp/oe2203.nbg/pkg:' $CONF
#sed -i 's;{architecture}/{compiler.name}-{compiler.version}/{name}-{version}-{hash};{name}/{version}_{hash:2};' $CONF

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc

echo nameserver 9.9.9.9 > rootfs/etc/resolv.conf

cat << 'EOP' > rootfs/tmp/oe2203.nbg/patch.sh
#! /bin/bash
SYSROOT=$PWD

find . -type f -executable | while read F ; do 
  if head -c 4 $F | grep -q ELF && patchelf --print-interpreter $F >&/dev/null; then
    patchelf --set-interpreter $SYSROOT/lib64/ld-linux-x86-64.so.2 $F 
    patchelf --set-rpath $SYSROOT/lib64:$SYSROOT/usr/lib64 $F
  fi
done

find . -type f -name 'lib*so*' | while read F; do
  if head -c 4 $F | grep -q ELF && patchelf --print-needed $F | grep -q lib; then
    patchelf --set-rpath $SYSROOT/lib64:$SYSROOT/usr/lib64 $F
  fi
done

$SYSROOT/usr/bin/gcc -dumpspecs | \
sed "/ld-linux-x86-64.so.2/s:/lib64/ld-linux-x86-64.so.2:$SYSROOT&:g" \
> $(dirname $($SYSROOT/usr/bin/gcc -print-libgcc-file-name))/specs
 
rm -f usr/bin/ld
echo -e "#! /bin/sh\nexec $SYSROOT/usr/bin/ld.bfd -rpath '\$ORIGIN/../lib:$SYSROOT/lib64:$SYSROOT/usr/lib64' \"\$@\""\
> usr/bin/ld
chmod +x usr/bin/ld

for F in gcc g++ c++ gfortran; do 
  mv usr/bin/$F usr/bin/$F.bin
  echo -e "#! /bin/sh\nexec $SYSROOT/usr/bin/$F.bin --sysroot=$SYSROOT \"\$@\"" > usr/bin/$F
  chmod +x usr/bin/$F
done

sed -i "1c#! $(which bash)" usr/bin/ldd
sed -i "/RTLDLIST=/cRTLDLIST=\"$SYSROOT/lib64/ld-linux-x86-64.so.2\"" usr/bin/ldd
EOP

cat << 'EOF' | chroot rootfs
cd /tmp/oe2203.nbg/
bash patch.sh
rm -rf /root/.spack
. /opt/python/bin/activate
export PATH=/tmp/oe2203.nbg/usr/bin:/opt/spack/bin:$PATH
spack compiler add
spack install zlib
EOF

umount rootfs/dev
umount rootfs/proc

cat $0 > rootfs/.tbr