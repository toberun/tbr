#! /bin/bash

mkdir rootfs
# QmTP57Nf9GVtbQHfe1B82iJQiLPVGsshosmMrxTB4cwBg8 voidlinux.20240619.aarch64.tar.xz
ipfs cat --progress=false QmTP57Nf9GVtbQHfe1B82iJQiLPVGsshosmMrxTB4cwBg8 | tar Jxf - -C rootfs
# Qmd799ZryHUBDjB1QBp3MEPTiRbau55mJKbQr7jtKGXMT8 spack-0.22.0.tar.gz
ipfs cat --progress=false Qmd799ZryHUBDjB1QBp3MEPTiRbau55mJKbQr7jtKGXMT8 | tar zxf - 
mv spack-0.22.0 rootfs/opt/spack

CONF=rootfs/opt/spack/etc/spack/defaults/config.yaml
#sed -i 's:$spack/opt/spack:/tmp/tbr.el8.nt:' $CONF
sed -i 's;{architecture}/{compiler.name}-{compiler.version}/{name}-{version}-{hash};{name}/{version}_{hash:2};' $CONF

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc


cat << 'EOF' | chroot rootfs
echo nameserver 114.114.114.114 > /etc/resolv.conf
#echo 'repository=https://mirrors.tuna.tsinghua.edu.cn/voidlinux/current/aarch64' > /etc/xbps.d/mirror.conf
xbps-install -Sy python3 patch which make curl tar bzip2 gzip xz unzip git gnupg autoconf automake gcc python3-devel libffi-devel
python3 -m venv /opt/python
/opt/python/bin/pip install clingo
. /opt/python/bin/activate
/opt/spack/bin/spack bootstrap now
EOF

umount rootfs/dev
umount rootfs/proc

cat $0 > rootfs/.tbr
# Qma4yf3uWtu5N48Za26UqFmjSKhEXqEwqvZmDNhr7rTgFp voidlinux.gcc.spack-0.22.aarch64.tar.xz