#! /bin/bash

mkdir rootfs
# Qma4yf3uWtu5N48Za26UqFmjSKhEXqEwqvZmDNhr7rTgFp voidlinux.gcc.spack-0.22.aarch64.tar.xz
ipfs cat --progress=false Qma4yf3uWtu5N48Za26UqFmjSKhEXqEwqvZmDNhr7rTgFp | tar Jxf - -C rootfs
rm -rf rootfs/tmp/*; mkdir -p rootfs/tmp/oe2203.nbg
# QmX61cjhd3PyPPdCCuKav2xg8LAB6pcg51LdgQQKWvpV91 openEuler-22.03-LTS-aarch64.sysroot.tar.xz
ipfs cat --progress=false QmX61cjhd3PyPPdCCuKav2xg8LAB6pcg51LdgQQKWvpV91 | tar Jxf - -C rootfs/tmp/oe2203.nbg
# QmUum56qrJw52HrWV9c6gxJAZiKhHNJQffWo2ciw91vAVv patchelf-0.15.0-aarch64
ipfs cat --progress=false QmUum56qrJw52HrWV9c6gxJAZiKhHNJQffWo2ciw91vAVv > rootfs/usr/local/bin/patchelf
chmod +x rootfs/usr/local/bin/patchelf

CONF=rootfs/opt/spack/etc/spack/defaults/config.yaml
sed -i 's:$spack/opt/spack:/tmp/oe2203.nbg/pkg:' $CONF
#sed -i 's;{architecture}/{compiler.name}-{compiler.version}/{name}-{version}-{hash};{name}/{version}_{hash:2};' $CONF

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc

echo nameserver 9.9.9.9 > rootfs/etc/resolv.conf

cat << 'EOP' > rootfs/tmp/oe2203.nbg/patch.sh
#! /bin/bash
SYSROOT=$PWD

find . -type f -executable | while read F ; do 
  if head -c 4 $F | grep -q ELF && patchelf --print-interpreter $F >&/dev/null; then
    patchelf --set-interpreter $SYSROOT/lib/ld-linux-aarch64.so.1 $F 
    patchelf --set-rpath $SYSROOT/lib64:$SYSROOT/usr/lib64 $F
  fi
done

find . -type f -name 'lib*so*' | while read F; do
  if head -c 4 $F | grep -q ELF && patchelf --print-needed $F | grep -q lib; then
    patchelf --set-rpath $SYSROOT/lib64:$SYSROOT/usr/lib64 $F
  fi
done

usr/bin/gcc -dumpspecs | \
sed "/ld-linux-aarch64/s:/lib/ld-linux-aarch64:$SYSROOT&:g" \
> $(dirname $(usr/bin/gcc -print-libgcc-file-name))/specs
 
rm -f usr/bin/ld
echo -e "#! /bin/sh\nexec $SYSROOT/usr/bin/ld.bfd -rpath '\$ORIGIN/../lib:$SYSROOT/lib64:$SYSROOT/usr/lib64' \"\$@\""\
> usr/bin/ld
chmod +x usr/bin/ld

for F in gcc g++ c++ gfortran; do 
  mv usr/bin/$F usr/bin/$F.bin
  echo -e "#! /bin/sh\nexec $SYSROOT/usr/bin/$F.bin --sysroot=$SYSROOT \"\$@\"" > usr/bin/$F
  chmod +x usr/bin/$F
done

sed -i "1c#! $(which bash)" usr/bin/ldd
sed -i "/RTLDLIST=/cRTLDLIST=\"$SYSROOT/lib/ld-linux-aarch64.so.1\"" usr/bin/ldd
EOP

cat << 'EOF' | chroot rootfs
xbps-remove -Ry gcc
cd /tmp/oe2203.nbg/
bash patch.sh
rm -f patch.sh
rm -rf /root/.spack
. /opt/python/bin/activate
export PATH=/tmp/oe2203.nbg/usr/bin:/opt/spack/bin:$PATH
spack compiler add
spack install zlib
EOF

umount rootfs/dev
umount rootfs/proc

cat $0 > rootfs/.tbr